/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ping.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/24 10:13:18 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/25 11:24:54 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PING_H
# define FT_PING_H

/* include struct icmphdr and iphdr */
# include <netinet/ip.h>
# include <netinet/ip_icmp.h>

/* include NULL */
# include <unistd.h>

/* include suseconds_t */
# include <sys/time.h>

# define HDRSIZE sizeof(struct icmphdr) + sizeof(struct iphdr)

struct		s_packet
{
	struct icmphdr	hdr;
	char		buf[56];
};

struct		s_recv
{
	struct iphdr	ip;
	struct icmphdr	icmp;
};

struct		s_ping_opt
{
	int		h;
	int		v;
	unsigned int	s;
	unsigned int	t;
};

struct		s_ping_info
{
	char			*host;
	char			*ip;
	struct s_ping_opt	opt;
	int			sock;
	int			pckt_send;
	int			pckt_recv;
	struct timeval		start_time;
	suseconds_t		running;
	float			min;
	float			max;
	double			avg;
	volatile int		timeout;
};

extern struct s_ping_info g_ping;

void		ft_usage(void);
int		socket_from_host(const char *host);
void		fill_opt(struct s_ping_opt *opt, int ac, char **av);
void		loop_ping(int sock);

/*
** Utils
*/

suseconds_t	get_tv_usec(void);
uint16_t	checksum(void *ptr, size_t len);

#endif
