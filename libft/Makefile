NAME		= 	libft.a
CC			= 	gcc
CFLAGS		= 	-Wall -Wextra -Werror -g

_ICLDS		= 	libft.h ft_string.h ft_stdio.h ft_ctype.h

_LIST_FS	= ft_lstadd.c ft_lstdel.c ft_lstdelone.c 	\
			  ft_lstiter.c ft_lstmap.c ft_lstnew.c 	\
			  ft_printlst.c ft_stack.c ft_queue.c ft_dequeue.c \
			  tree.c

_STRING_FS	= ft_strchr.c ft_strclr.c ft_strcat.c ft_strdel.c 	\
			  ft_strcmp.c ft_strcpy.c ft_striteri.c ft_strequ.c 				\
			  ft_striter.c ft_strdup.c ft_strlen.c ft_strjoin.c 				\
			  ft_strlcat.c ft_strmap.c ft_strncmp.c ft_strmapi.c 				\
			  ft_strncat.c ft_strncpy.c ft_strnew.c ft_strnequ.c 				\
			  ft_strrchr.c ft_strnstr.c ft_strrange.c ft_strswap.c 			\
			  ft_strsub.c ft_strstr.c ft_strtrim.c ft_strsplit.c ft_strset.c \
			  ft_strnchr.c ft_strwlen.c ft_memset.c ft_memalloc.c \
			  ft_memchr.c ft_memdel.c ft_memmove.c ft_memcpy.c ft_memcmp.c \
			  ft_memccpy.c ft_bzero.c ft_free_tab.c ft_strremove.c \
			  ft_tablen.c ft_strlower.c ft_strmsplit.c

_STDIO_FS	= ft_printtab.c ft_putchar.c 			\
			  ft_putchar_fd.c ft_putendl.c ft_putendl_fd.c 		\
			  ft_putstr.c ft_putstr_fd.c ft_putnbr.c ft_putnbr_pos.c ft_putnbr_fd.c 	\
			  ft_putwchar.c ft_putwstr.c ft_putc.c \
			  get_next_line.c ft_itoa.c

_CTYPE_FS	= ft_toupper.c ft_tolower.c ft_isalnum.c \
			  ft_isascii.c ft_isalpha.c ft_isdigit.c 			\
			  ft_isprint.c ft_isws.c change_endian.c

_STDLIB_FS	= ft_itoa_base.c ft_intlen.c 			\
			  ft_count_bytes.c  		\
			  ft_itoa_base_u.c ft_atoi.c ft_charswap.c \
			  ft_countwords.c ft_rand.c ft_getopt.c

SRCD		= 	srcs/
ICLD		= 	includes/
ICLDS		=	$(addprefix $(ICLD),$(_ICLDS))

LIST_FS		=	$(addprefix $(SRCD),$(_LIST_FS))
_LIST_FSO	=	$(_LIST_FS:.c=.o)
LIST_FSO	=	$(LIST_FS:.c=.o)

STRING_FS	=	$(addprefix $(SRCD)ft_string/,$(_STRING_FS))
_STRING_FSO	=	$(_STRING_FS:.c=.o)
STRING_FSO	=	$(STRING_FS:.c=.o)

STDIO_FS	=	$(addprefix $(SRCD)ft_stdio/,$(_STDIO_FS))
_STDIO_FSO	=	$(_STDIO_FS:.c=.o)
STDIO_FSO	=	$(STDIO_FS:.c=.o)

CTYPE_FS	=	$(addprefix $(SRCD)ft_ctype/,$(_CTYPE_FS))
_CTYPE_FSO	=	$(_CTYPE_FS:.c=.o)
CTYPE_FSO	=	$(CTYPE_FS:.c=.o)

STDLIB_FS	=	$(addprefix $(SRCD)ft_stdlib/,$(_STDLIB_FS))
_STDLIB_FSO	=	$(_STDLIB_FS:.c=.o)
STDLIB_FSO	=	$(STDLIB_FS:.c=.o)

SRCS		=	$(LIST_FS) $(STRING_FS) $(STDIO_FS) $(CTYPE_FS)			\
				$(STDLIB_FS)
_OBJS		=	$(_LIST_FSO) $(_STRING_FSO) $(_STDIO_FSO) $(_CTYPE_FSO)	\
				$(_STDLIB_FSO)
OBJD		=	.objs/
OBJS		=	$(LIST_FSO) $(STRING_FSO) $(STDIO_FSO) $(CTYPE_FSO)		\
				$(STDLIB_FSO)
OBJB		=	$(addprefix $(OBJD),$(_OBJS))

# COLORS
CRED=\033[91m
CGREEN=\033[38;2;0;255;145m
CEND=\033[0m

all: $(NAME)

$(NAME): $(OBJB)
	@printf "\r\033[K$(CGREEN)Creating library$(CEND): $(NAME)\n"
	@ar rc $(NAME) $(OBJB)
	@ranlib $(NAME)
	@echo  "$(NAME): $(CGREEN)done$(CEND)"

$(OBJD)%.o: $(SRCD)%.c $(ICLDS) Makefile
	@printf "\r\033[K$(CGREEN)Compiling$(CEND): $<"
	@mkdir -p $(OBJD)
	@$(CC) $(CFLAGS) -o $@ -c $< -I$(ICLD)

$(OBJD)%.o: $(SRCD)ft_string/%.c $(ICLDS) Makefile
	@printf "\r\033[K$(CGREEN)Compiling$(CEND): $<"
	@mkdir -p $(OBJD)
	@$(CC) $(CFLAGS) -o $@ -c $< -I$(ICLD)

$(OBJD)%.o: $(SRCD)ft_stdio/%.c $(ICLDS) Makefile
	@printf "\r\033[K$(CGREEN)Compiling$(CEND): $<"
	@mkdir -p $(OBJD)
	@$(CC) $(CFLAGS) -o $@ -c $< -I$(ICLD)

$(OBJD)%.o: $(SRCD)ft_ctype/%.c $(ICLDS) Makefile
	@printf "\r\033[K$(CGREEN)Compiling$(CEND): $<"
	@mkdir -p $(OBJD)
	@$(CC) $(CFLAGS) -o $@ -c $< -I$(ICLD)

$(OBJD)%.o: $(SRCD)ft_stdlib/%.c $(ICLDS) Makefile
	@printf "\r\033[K$(CGREEN)Compiling$(CEND): $<"
	@mkdir -p $(OBJD)
	@$(CC) $(CFLAGS) -o $@ -c $< -I$(ICLD)

$(OBJD)%.o: $(SRCD)ft_list/%.c $(ICLDS) Makefile
	@printf "\r\033[K$(CGREEN)Compiling$(CEND): $<"
	@mkdir -p $(OBJD)
	@$(CC) $(CFLAGS) -o $@ -c $< -I$(ICLD)

clean:
	@rm -f $(OBJB)
	@echo "$(CRED)Cleaning$(CEND): $(NAME)"

fclean: clean
	@rm -f $(NAME)
	@echo "$(CRED)Full cleaning$(CEND): $(NAME)"

re: fclean all

.PHONY: tests
tests: $(NAME)
	@make -s -C tests
	@./tests/tests

.PHONY: all clean fclean re
