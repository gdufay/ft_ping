/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   couleurs.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 10:38:43 by gdufay            #+#    #+#             */
/*   Updated: 2018/01/18 11:20:00 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COULEURS_H
# define COULEURS_H

# define CHNG_CLR "\033[%sm"
# define P_BLACK "30"
# define P_RED "31"
# define P_GREEN "32"
# define P_YELLOW "33"
# define P_BLUE "34"
# define P_MAGEN "35"
# define P_CYAN "36"
# define P_WHITE "37"
# define F_BLACK "40"
# define F_RED "41"
# define F_GREEN "43"
# define F_YELLOW "42"
# define F_BLUE "44"
# define F_MAGEN "45"
# define F_CYAN "46"
# define F_WHITE "47"
# define RESET "0"

#endif
