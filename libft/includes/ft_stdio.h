/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stdio.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/05 13:56:35 by gdufay            #+#    #+#             */
/*   Updated: 2019/07/24 10:08:09 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STDIO_H
# define FT_STDIO_H

# include <unistd.h>
# include <stddef.h>

# include "libft.h"

# define BUF_SIZE 1024

int					ft_putc(int c);
void				ft_putchar(char c);
void				ft_putstr(char const *str);
void				ft_putnbr(int c);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);
void				ft_putnbr_pos(size_t nb);
void				ft_putendl(char const *s);
void				ft_putendl_fd(char const *s, int fd);
void				ft_printtab(char **tabi);
void				ft_putwchar(wchar_t c);
void				ft_putwstr(wchar_t *s);

int					get_next_line(int fd, char **line);

#endif
