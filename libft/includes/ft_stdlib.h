/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stdlib.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/05 14:40:14 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/24 11:03:38 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STDLIB_H
# define FT_STDLIB_H

# include <stdlib.h>
# include <fcntl.h>

int				ft_atoi(const char *str);
int				ft_intlen(long long int n);
int				ft_countwords(const char *s, char c);
int				ft_count_bytes(wchar_t c);
void			ft_charswap(char *a, char *b);
char			*ft_itoa(int n);
char			*ft_itoa_base(intmax_t n, int base, int up);
char			*ft_itoa_base_u(size_t n, int base, int up);
char			*ft_itoa_base_begin_with(size_t n, int base, size_t min,
		int fill);
int				ft_rand(void);

/*
** global var for ft_getopt
*/

extern char		*g_optarg;
extern int		g_optind;
extern int 		g_optopt;
extern int 		g_opterr;

int				ft_getopt(int ac, char *const av[], const char *opstring);

#endif
