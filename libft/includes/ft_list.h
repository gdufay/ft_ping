/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/05 14:50:21 by gdufay            #+#    #+#             */
/*   Updated: 2019/06/12 14:22:58 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIST_H
# define FT_LIST_H

# include "libft.h"

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

t_list				*ft_lstnew(void const *content, size_t content_size);
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void				ft_lstadd(t_list **alst, t_list *new);
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
void				ft_printlst(t_list *lst);

/*
 * *********** STACK *****************
*/

int					ft_stackisempty(t_list *stack);
void				*ft_stackpeek(t_list *stack);
int					ft_stackpush(t_list **stack, t_list *elem);
t_list				*ft_stackpop(t_list **stack);
void				ft_printstack(t_list *stack);

/*
 * *********** QUEUE *****************
*/

typedef	struct		s_queue
{
	t_list *front;
	t_list *rear;
}					t_queue;

t_queue				*ft_queuenew(t_list *front, t_list *rear);
int					ft_enqueue(t_queue *queue, t_list *elem);
t_list				*ft_dequeue(t_queue *queue);

/*
 * *********** DEQUEUE *****************
*/

int					ft_dq_insertfront(t_queue *dequeue, t_list *elem);
t_list				*ft_dq_deletefront(t_queue *dequeue);
int					ft_dq_insertrear(t_queue *dequeue, t_list *elem);

/*
 * *********** TREE *****************
*/

typedef struct		s_tree
{
	void			*content;
	size_t			content_size;
	struct s_tree	*left;
	struct s_tree	*right;
}					t_tree;

t_tree				*ft_treenew(void *content, size_t content_size);
void				ft_treeadd(t_tree *tree, t_tree *node,
		int (*f)(t_tree*, t_tree*));
void				ft_printtree(t_tree *root, void (*print)(t_tree*));
void				ft_treeclear(t_tree *root, void (*ft_free)(t_tree*));

#endif
