/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/24 10:25:07 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/24 10:50:12 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tests.h"

/*
** TODO: create a verbose mode
*/

int		main(int ac, char *av[])
{
	const t_tests	tests[] = {
		SMART_ENUM_TESTS(ASSOC)
	};
	size_t			i;

	i = -1;
	if (ac == 1)
		while (++i < sizeof(tests) / sizeof(*tests))
			tests[i].ft();
	else
		while (*++av)
			while (++i < sizeof(tests) / sizeof(*tests))
				if (!strcmp(*av, tests[i].name))
					tests[i].ft();
	return (0);
}
