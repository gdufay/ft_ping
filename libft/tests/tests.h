/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tests.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/24 10:24:34 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/24 10:52:52 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TESTS_H
# define TESTS_H

# include "libft.h"
# include <string.h>
# include <stdio.h>

/* Enum containing all the tested functions */

# define SMART_ENUM_TESTS(_)\
	_(getopt)

/* Add prefix test_ to var x */
# define PREFIX(x) test_ ## x

/* Create member of struct s_tests */
# define ASSOC(x) { #x, PREFIX(x) },

/* Declare function prototype */
# define DECLARE_FT(x) int PREFIX(x)(void);

typedef	struct		s_tests
{
	char	*name;
	int		(*ft)(void);
}					t_tests;

SMART_ENUM_TESTS(DECLARE_FT)

#endif
