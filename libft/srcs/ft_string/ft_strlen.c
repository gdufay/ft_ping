/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 15:04:29 by gdufay            #+#    #+#             */
/*   Updated: 2019/06/12 11:10:39 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlen(const char *s1)
{
	size_t i;

	i = 0;
	if (s1)
		while (*s1++)
			i++;
	return (i);
}

size_t		ft_strnlen(const char *s1, size_t maxlen)
{
	size_t i;

	i = 0;
	if (s1)
		while (maxlen-- && *s1++)
			i++;
	return (i);
}
