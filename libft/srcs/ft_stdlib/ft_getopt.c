/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getopt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/24 10:57:03 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/24 15:19:36 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*g_optarg = NULL;
int		g_optind = 1;
int		g_optopt = 0;

static int	find_opt(char *const av[], char *s, char *chr, char **nextchar)
{
	if (chr && *(chr + 1) == ':')
	{
		if (*s)
			g_optarg = s;
		else
			g_optarg = av[++g_optind];
		g_optind++;
		*nextchar = NULL;
		if (g_optarg == NULL)
			return (':');
	}
	else
	{
		if (*s)
			*nextchar = s;
		else
		{
			*nextchar = NULL;
			g_optind++;
		}
	}
	return (chr ? g_optopt : '?');
}

int			ft_getopt(int ac, char *const av[], const char *opstring)
{
	static char	*nextchar = NULL;
	char		*chr;
	char		*s;

	g_optarg = NULL;
	if (g_optind > ac || av[g_optind] == NULL)
		return (-1);
	if (nextchar)
		s = nextchar;
	else if (*(av[g_optind]) != '-')
		return (-1);
	else
		s = av[g_optind] + 1;
	g_optopt = *s;
	if (*s == '-')
	{
		g_optind++;
		return (-1);
	}
	if ((chr = ft_strchr(opstring, *s)))
		return (find_opt(av, ++s, chr, &nextchar));
	else
		return (find_opt(av, ++s, NULL, &nextchar));
}
