/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rand.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/17 12:28:02 by gdufay            #+#    #+#             */
/*   Updated: 2018/10/17 13:24:33 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#define PATH "/dev/urandom"

int		ft_rand(void)
{
	int		fd;
	int		buf[1];

	if ((fd = open(PATH, O_RDONLY)) == -1)
		return (-1);
	if (read(fd, buf, sizeof(*buf)) == -1)
	{
		close(fd);
		return (-1);
	}
	close(fd);
	return (!(buf[0] & 1));
}
