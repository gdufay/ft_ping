/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stack.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/18 13:24:38 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/24 14:41:46 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
**void	ft_printstack(t_list *stack)
**{
**	char	*address;
**
**	while (stack)
**	{
**		ft_putstr("| address: 0x");
**		address = ft_itoa_base((unsigned int)stack, 16, 0);
**		ft_putstr(address);
**		ft_strdel(&address);
**		ft_putstr(" content: 0x");
**		address = ft_itoa_base((unsigned int)(stack->content), 16, 0);
**		ft_putstr(address);
**		ft_strdel(&address);
**		ft_putendl(" |");
**		stack = stack->next;
**	}
**}
*/

t_list	*ft_stackpop(t_list **stack)
{
	t_list *tmp;

	if (!stack)
		return (NULL);
	tmp = *stack;
	if (*stack)
		*stack = (*stack)->next;
	return (tmp);
}

int		ft_stackpush(t_list **stack, t_list *elem)
{
	if (!stack || !elem)
		return (0);
	elem->next = *stack;
	*stack = elem;
	return (1);
}

void	*ft_stackpeek(t_list *stack)
{
	return (stack ? stack->content : NULL);
}

int		ft_stackisempty(t_list *stack)
{
	return (!stack);
}
