/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tree.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/31 13:37:23 by gdufay            #+#    #+#             */
/*   Updated: 2019/06/12 14:25:16 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_tree		*ft_treenew(void *content, size_t content_size)
{
	t_tree	*node;

	if (!(node = malloc(sizeof(t_tree))))
		return (NULL);
	node->content = content;
	node->content_size = content_size;
	node->left = NULL;
	node->right = NULL;
	return (node);
}

/*
** left: f = true
** right: f = false
*/

/*
 ** treeadd un peu moche
*/

void		ft_treeadd(t_tree *tree, t_tree *node, int (*f)(t_tree*, t_tree*))
{
	int		cmp;

	if (!tree)
		return ;
	cmp = f(tree, node);
	if (cmp < 0)
	{
		if (!tree->left)
			tree->left = node;
		else
			ft_treeadd(tree->left, node, f);
	}
	else if (cmp > 0)
	{
		if (!tree->right)
			tree->right = node;
		else
			ft_treeadd(tree->right, node, f);
	}
}

void		ft_printtree(t_tree *root, void (*print)(t_tree*))
{
	if (!root)
		return ;
	if (root->left)
		ft_printtree(root->left, print);
	print(root);
	if (root->right)
		ft_printtree(root->right, print);
}

void		ft_treeclear(t_tree *tree, void (*ft_free)(t_tree*))
{
	if (!tree)
		return ;
	if (tree->left)
		ft_treeclear(tree->left, ft_free);
	if (tree->right)
		ft_treeclear(tree->right, ft_free);
	ft_free(tree);
	free(tree);
}
