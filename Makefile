NAME = ft_ping

CC = gcc
CFLAGS = -Wall -Wextra -Werror -g 

ICLD = includes/
_INCLUDES = ft_ping.h
INCLUDES = $(addprefix $(ICLD), $(_INCLUDES))
LIB_ICLD = libft/includes/

SRCD = srcs/

_SRCS = main.c usage.c socket_from_host.c ping_opt.c loop.c	\
	utils.c
SRCS = $(addprefix $(SRCD), $(_SRCS))
OBJS = $(SRCS:.c=.o)

LIBRARIES = libft/libft.a

# COLORS
CRED=\033[91m
CGREEN=\033[38;2;0;255;145m
CEND=\033[0m

.PHONY: all
all: $(NAME)

.PHONY: libft
libft:
	@make -j -s -C libft

$(NAME): $(OBJS)
	@printf "\r\033[K$(CGREEN)Creating client$(CEND): $(NAME)\n"
	@$(CC) $(CFLAGS) -o $@ $^ $(LIBRARIES)
	@echo "$(NAME): $(CGREEN)done$(CEND)"

%.o: %.c $(INCLUDES) libft
	@printf "\r\033[K$(CGREEN)Compiling$(CEND): $<"
	@$(CC) $(CFLAGS) -o $@ -c $< -I$(ICLD) -I$(LIB_ICLD)

.PHONY: clean
clean:
	@echo "$(CRED)Cleaning$(CEND): $(NAME)"
	@rm -f $(OBJS)
	@make -s -C libft clean

.PHONY: fclean
fclean: clean
	@echo "$(CRED)Full cleaning$(CEND): $(NAME)"
	@rm -f $(NAME)
	@make -s -C libft fclean

.PHONY: re
re: fclean all
