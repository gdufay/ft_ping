/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/24 10:13:56 by gdufay            #+#    #+#             */
/*   Updated: 2019/09/25 11:45:59 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"
#include "libft.h"
#include <unistd.h>
#include <stdio.h>
#include <float.h>
#include <time.h>

// TODO: define return value

struct s_ping_info g_ping = {
	.host = NULL,
	.ip = NULL,
	.sock = -1,
	.pckt_send = 0,
	.pckt_recv = 0,
	.min = FLT_MAX,
	.max = 0,
	.avg = 0,
	.running = 0,
};

int		main(int ac, char *av[])
{
	int		sock;
	struct timeval	tv;

	if (ac < 2) {
		ft_usage();
		return (1);
	}
	if (getuid() != 0) {
		ft_putstr("Need to be root\n");
		return (1);
	}
	ft_bzero((void*)&g_ping.opt, sizeof(g_ping.opt));
	g_ping.opt.s = 56;
	g_ping.opt.t = IPDEFTTL;
	fill_opt(&g_ping.opt, ac, av);
	g_ping.host = av[g_optind];
	if ((sock = socket_from_host(av[g_optind])) == -1) {
		ft_putstr("Bad host\n");
		return (1);
	}
	g_ping.sock = sock;
	tv.tv_sec = 0;
	tv.tv_usec = 1000;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
	setsockopt(sock, SOL_IP, IP_TTL, &g_ping.opt.t, sizeof(g_ping.opt.t));
	printf("PING %s (IP) %u(%zu) bytes of data\n", av[g_optind], g_ping.opt.s, g_ping.opt.s + HDRSIZE);
	loop_ping(sock);
	close(sock);
	return (0);
}
