#include "ft_ping.h"
#include <sys/time.h>

suseconds_t	get_tv_usec(void)
{
	struct timeval	time;

	if (gettimeofday(&time, NULL) == -1)
		return (0);
	return (time.tv_usec);
}

uint16_t	checksum(void *ptr, size_t len)
{
	int		sum = 0;
	uint16_t	*cast = ptr;
	uint16_t	ret;

	while (len > 1) {
		sum += *cast++;
		len -= 2;
	}
	if (len == 1)
		sum += *(uint8_t*)cast;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	ret = ~sum;
	return (ret);
}
