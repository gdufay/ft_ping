#include "ft_ping.h"
#include "libft.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <signal.h>

static void	prepare_rtt(float diff)
{
	if (diff > g_ping.max)
		g_ping.max = diff;
	if (diff < g_ping.min)
		g_ping.min = diff;
	g_ping.avg += diff;
}

static int	send_ping(int sock)
{
	struct s_packet	pckt;

	pckt.hdr.type = ICMP_ECHO;
	pckt.hdr.code = 0;
	pckt.hdr.un.echo.id = getpid();
	pckt.hdr.un.echo.sequence = ++g_ping.pckt_send;
	ft_memset(pckt.buf, 'A', sizeof(pckt.buf) - 1);
	pckt.hdr.checksum = checksum(&pckt, sizeof(pckt));
	g_ping.running = get_tv_usec();
	sendto(sock, &pckt, sizeof(pckt), 0, NULL, 0);
	return (0);
}

// TODO: check checksum when receiv

static int	recv_ping(int sock)
{
	struct s_recv	rec;
	struct msghdr	msg;
	struct iovec	iov = {&rec, sizeof(rec)};
	float		diff;
	char		buf[64];

	ft_bzero(&msg, sizeof(msg));
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	recvmsg(sock, &msg, 0);
	inet_ntop(AF_INET, &rec.ip.saddr, buf, 64);
	if (rec.icmp.type == ICMP_ECHOREPLY) {
		g_ping.pckt_recv++;
		diff = (get_tv_usec() - g_ping.running) / 1000.0;
		printf("%zu bytes from %u (%s): icmp_seq=%u ttl=%d time=%.2fms\n", sizeof(struct s_packet), rec.ip.saddr, buf, g_ping.pckt_send, rec.ip.ttl, diff);
		prepare_rtt(diff);
	}
	else {
		printf("%zu bytes from %s: type = %d, code = %d\n", sizeof(struct s_packet), buf, rec.icmp.type, rec.icmp.code);
		printf("TTL: %d\n", rec.ip.ttl);
	}
	return (0);
}

static void	print_stats(void)
{
	size_t		elapsed;
	int		loss_percent;
	struct timeval	end;

	gettimeofday(&end, NULL);
	elapsed = (end.tv_sec - g_ping.start_time.tv_sec) * 1000 + (end.tv_usec - g_ping.start_time.tv_usec) / 1000;
	loss_percent = -(g_ping.pckt_recv - g_ping.pckt_send) / g_ping.pckt_send * 100;
	printf("\n--- %s ping statistics ---\n", g_ping.host);
	printf("%d packets transmitted, %d received, %d%% packet loss, time %zums\n", g_ping.pckt_send, g_ping.pckt_recv, loss_percent, elapsed);
	if (g_ping.pckt_recv)
		printf("rtt min/avg/max/mdev = %.3f/%.3f/%.3f/mdev ms\n", g_ping.min, g_ping.avg / g_ping.pckt_recv, g_ping.max);
}

static void	sighandler(int sig)
{
	switch (sig) {
		case SIGINT:
			print_stats();
			exit(EXIT_SUCCESS);
			break ;
		case SIGALRM:
			g_ping.timeout = 0;
			break ;
		default:
			break ;
	}
}

void	loop_ping(int sock)
{
	gettimeofday(&g_ping.start_time, NULL);
	signal(SIGINT, sighandler);
	signal(SIGALRM, sighandler);
	while (42) {
		send_ping(sock);
		recv_ping(sock);
		alarm(1);
		g_ping.timeout = 1;
		while (g_ping.timeout) ;
	}
}
