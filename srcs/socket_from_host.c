#include "ft_ping.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

// TODO: add opts and handle them for bonus

int		socket_from_host(const char *host)
{
	struct addrinfo hints = {
		.ai_family = AF_UNSPEC,
		.ai_socktype = SOCK_RAW,
		.ai_flags = 0,
		.ai_protocol = IPPROTO_ICMP,
	};
	struct addrinfo	*res, *loop;
	int		sock;

	if (getaddrinfo(host, NULL, &hints, &res))
		return (-1);
	sock = -1;
	for (loop = res; loop != NULL; loop = loop->ai_next)
	{
		sock = socket(loop->ai_family, loop->ai_socktype, loop->ai_protocol);
		if (sock == -1)
			continue ;
		if (connect(sock, loop->ai_addr, loop->ai_addrlen) != -1)
			break ;
		close(sock);
	}
	freeaddrinfo(res);
	return (sock);
}
