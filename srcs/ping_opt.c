#include "ft_ping.h"
#include "libft.h"
#include <stdio.h>

void	fill_opt(struct s_ping_opt *opt, int ac, char **av)
{
	int					ch;

	while ((ch = ft_getopt(ac, av, "vhs:t:")) != -1)
	{
		switch (ch) {
			case 'h':
				ft_usage();
				exit(EXIT_SUCCESS);
				break ;
			case 'v':
				opt->v = 1;
				break ;
			case 's':
				opt->s = (unsigned int)ft_atoi(g_optarg);
				break ;
			case 't':
				opt->t = (unsigned int)ft_atoi(g_optarg);
				break ;
			default:
				dprintf(2, "ft_ping: invalid option -- '%c'\n", g_optopt);
				ft_usage();
				exit(EXIT_FAILURE);
		}
	}
}
